# WIfPred - The What If Predictor

Given the past, model rulesets and some sensible estimate options, wifpred will analyse potential future outcomes. This could be anything involving discrete steps, be it retirement calculations or the weather forecast.